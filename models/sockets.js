const BandList = require("./band-list");

class Sockets {
    constructor(io) {
        this.io = io;
        this.bandList = new BandList();
        this.socketEvents();
    }

    socketEvents() {
        // On connection
        this.io.on("connection", (socket) => {
            // Emit connected client all bands
            socket.emit('All-bands', this.bandList.getBands())
            socket.on('vote-band', (id) => {
                this.bandList.increaseVote(id)
                this.io.emit('All-bands', this.bandList.getBands())
            })
            socket.on('delete-band', id => {
                this.bandList.removeBand(id)
                this.io.emit('All-bands', this.bandList.getBands())
            })

            socket.on('update-band', (data) => {
                this.bandList.changeName(data.id, data.name)
                this.io.emit('All-bands', this.bandList.getBands())
            })

            socket.on('create-band', (name) => {
                this.bandList.addBand(name)
                console.log('Band added', name)
                this.io.emit('All-bands', this.bandList.getBands())
            })
        });
    }
}

module.exports = Sockets;
