import React, { useState, useEffect, useContext } from "react";
import { SocketContext } from "../context/SocketContext";

export const BandList = () => {
  const { socket } = useContext(SocketContext);
  const [band, setBand] = useState([]);

  useEffect(() => {
    socket.on("All-bands", (bands) => {
      setBand(bands);
    });
  }, [socket]);

  const vote = (id) => {
    socket.emit("vote-band", id);
  };

  const deleteBand = (id) => {
    socket.emit("delete-band", id);
  };

  const nameChanged = (e, id) => {
    const newName = e.target.value;
    setBand((band) =>
      band.map((band) => {
        if (band.id === id) band.name = newName;
        return band;
      })
    );
  };

  const onUnfocus = (id, name) => {
    updateName(id, name);
  };

  const updateName = (id, name) => {
    socket.emit("update-band", { id, name });
  };

  const createRows = () => {
    return band.map((band) => (
      <tr key={band.id}>
        <td>
          <button className="btn btn-primary" onClick={() => vote(band.id)}>
            {" "}
            +1
          </button>
          <button className="btn btn-secondary"> -1</button>
        </td>
        <td>
          <input
            type="text"
            className="form-control"
            value={band.name}
            onChange={(e) => nameChanged(e, band.id)}
            onBlur={() => onUnfocus(band.id, band.name)}
          />
        </td>
        <td>
          <h4>{band.votes}</h4>
        </td>
        <td>
          <button
            className="btn btn-danger"
            onClick={() => deleteBand(band.id)}
          >
            Delete
          </button>
        </td>
      </tr>
    ));
  };
  return (
    <>
      <table className="table table-stripped">
        <thead>
          <tr>
            <th>Vote</th>
            <th>Name</th>
            <th>Votes</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>{createRows()}</tbody>
      </table>
    </>
  );
};
