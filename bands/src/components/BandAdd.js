import React, { useContext, useState } from "react";
import { SocketContext } from "../context/SocketContext";

export const BandAdd = () => {
  const [name, setName] = useState();
  const { socket } = useContext(SocketContext);

  const onSubmit = (e) => {
    e.preventDefault();
    socket.emit("create-band", name);
    setName("");
  };

  const onChange = (e) => {
    const name = e.target.value;
    setName(name);
  };
  return (
    <h3>
      <h3>
        Agregar Banda: <b>{name}</b>
      </h3>
      <form onSubmit={onSubmit}>
        <input
          type="text"
          className="form-control"
          placeholder="Agregar nombre"
          onChange={onChange}
          value={name}
        />
      </form>
    </h3>
  );
};
