import React from "react";
import { SocketProvider } from "./context/SocketContext";
import HomePage from "./pages/HomePage";

export const BandNamesApp = () => {
  return (
    <div>
      <SocketProvider>
        <HomePage />
      </SocketProvider>
    </div>
  );
};
