import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./pages/HomePage";
import reportWebVitals from "./reportWebVitals";
import { BandNamesApp } from "./BandNamesApp";

ReactDOM.render(
  <React.StrictMode>
    {/* <App/> */}
    <BandNamesApp />
  </React.StrictMode>,
  document.getElementById("root")
);
reportWebVitals();
